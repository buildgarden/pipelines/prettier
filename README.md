# prettier pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/prettier?branch=main)](https://gitlab.com/buildgarden/pipelines/prettier/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/prettier)](https://gitlab.com/buildgarden/pipelines/prettier/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![cici-tools Enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Format things with [Prettier](https://prettier.io/).

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Usage

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/prettier
    file: prettier.yml
```

The `PRETTIER_PACKAGES` variable can be used to install extra NPM packages
before running Prettier:

```yaml
variables:
  PRETTIER_PACKAGES: >-
    @ianvs/prettier-plugin-sort-imports
```
